<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hitung Vokal</title>
</head>

<body>
    <form action="<?php $PHP_SELF; ?>" method="post">
        <p>Kata:
            <input type="text" name="nama" id="">
            <button type="submit" name="submit">Hitung</button>
        </p>
    </form>

    <?php
    function hitungVokal($nama)
    {
        $arr = str_split($nama);
        $vokal = ['a', 'i', 'u', 'e', 'o'];

        $found = array_intersect($vokal, $arr);

        if (count($found) >= 0) {
            echo $nama . " = " . count($found) . " yaitu ";
            $i = 0;
            foreach ($found as $f) {
                $i++;
                echo $f . ", ";
            }
        }
    }

    if (isset($_POST['submit'])) {
        $nama = $_POST['nama'];
        hitungVokal($nama);
    }
    ?>


</body>

</html>