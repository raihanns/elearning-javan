# Tugas 1: Basic Programming
## 1.1 Cetak Ganjil Genap
![Cetak_Ganjil_Genap](/uploads/de71a15cfbaa73c04cb8205993f447c2/Cetak_Ganjil_Genap.jpg)
## 1.2 Hitung Vokal
![Hitung_Vokal](/uploads/2e0b57406878fcba15dd85cb36f68433/Hitung_Vokal.jpg)
## 1.3 Kalkulator Sederhana
![Kalkulator](/uploads/e54ce152a8ae9243d8fd8299eda8cf95/Kalkulator.jpg)
![Kalkulator_2](/uploads/d971162287a1d0a6ac0f619e70d87ba0/Kalkulator_2.jpg)
