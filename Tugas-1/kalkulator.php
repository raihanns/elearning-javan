<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kalkulator Sederhana</title>
</head>

<body>
    <div>
        <h2>Kalkulator Sederhana</h2>
        <form method="post" action="<?php $PHP_SELF; ?>">
            <input type="text" name="angka" class="angka" autocomplete="off" placeholder="Masukkan Bilangan">
            <input type="submit" name="hitung" value="Hitung" class="tombol">
        </form>


        <?php
        function kalkulator($angka)
        {
            $angka = str_replace(" ", "", $angka);

            //array untuk operator dan mengecek lalu memasukkannya ke dalam array
            $operator = array();
            for ($i = 0; $i <= strlen($angka) - 1; $i++) {
                if (($angka[$i] == "+") || ($angka[$i] == "-") || ($angka[$i] == "*") || ($angka[$i] == "/")) {
                    $operator[] = $angka[$i];
                }
            }

            $angka = str_replace("+", " ", $angka);
            $angka = str_replace("x", " ", $angka);
            $angka = str_replace("-", " ", $angka);
            $angka = str_replace("/", " ", $angka);

            $operand = explode(" ", $angka);
            $hasil = $operand[0];

            for ($i = 0; $i <= count($operator) - 1; $i++) {
                if ($operator[$i] == "+") $hasil = $hasil + $operand[$i + 1];
                else if ($operator[$i] == "-") $hasil = $hasil - $operand[$i + 1];
                else if ($operator[$i] == "x") $hasil = $hasil * $operand[$i + 1];
                else if ($operator[$i] == "/") {
                    if ($operand[$i + 1] == 0) {
                        $hasil = "tidak bisa dilakukan";
                    } else {
                        $hasil = $hasil / $operand[$i + 1];
                    }
                };
            }

            echo "<br>Hasil perhitungannya adalah: " . $hasil;
        }

        if (isset($_POST['hitung'])) {
            $angka = $_POST['angka'];
            kalkulator($angka);
        }
        ?>

    </div>
</body>

</html>