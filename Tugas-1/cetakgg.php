<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cetak Ganjil Genap - Magang Programmer PHP</title>
</head>

<body>
    <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
        <br><br>
        <input type="text" name="bil1" placeholder="Start From">
        <input type="text" name="bil2" placeholder="To">
        <input type="submit" name="submit" value="Submit">
    </form>
    <?php

    $tampil = "";
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $bil1 = $_POST['bil1'];
        $bil2 = $_POST['bil2'];
        foreach (range($bil1, $bil2) as $bil) {
            if ($bil % 2 == 0) {
                echo "<br> Angka $bil adalah Genap <br>";
            } else {
                echo "<br> Angka $bil adalah Ganjil <br>";
            }
        }
    }
    ?>

</body>

</html>